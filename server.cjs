// NOTE: Your server must be named `server.cjs`
// All JS files must have a `.cjs` extension

/*  
    Create POST routes for the following:
        - Create a directory of random JSON files with the directory name and list of files to be created being taken from the request body.

    Create DELETE routes for the following:
        - Delete the files mentioned in the request body. Directory name must also be provided in the request body.
    
    General instructions: 

    - All responses must be sent only when everything was successful or not. If there is a partial success or partial failure, you may send a different response for that.

    - Response JSON structure must be consistent.

    - Error handling is extremely important.
*/

const express = require('express');
const serverApp = express();
const fs = require('fs');
const path = require('path');
const PORT = process.env.PORT || 8000;


serverApp.use(express.json());

function directoryCreater(directoryName) {

    return new Promise((resolve, rejects) => {

        fs.mkdir(path.join(__dirname, directoryName), (error) => {

            if (error) {

                rejects(error);

            } else {

                resolve(`${directoryName} created`);

            }

        });

    });

}

function fileCreater(fileName, directoryName) {


    return new Promise((rejects, resolve) => {

        if (fs.existsSync(path.join(__dirname, directoryName, `${fileName}.json`))) {

            resolve(`${fileName}.json already exists`);

        } else {

            fs.writeFile(path.join(__dirname, directoryName, `${fileName}.json`), JSON.stringify({ "name": "Sanjeev" }), (error) => {

                if (error) {

                    rejects(error);

                } else {

                    resolve(`${fileName}.json created`);

                }

            });

        }

    });

}

function fileDeleter(fileName, directoryName) {

    return new Promise((resolve, rejects) => {

        if (fs.existsSync(path.join(__dirname, directoryName, fileName))) {

            fs.unlink(path.join(__dirname, directoryName, fileName), (error) => {

                if (error) {

                    rejects(error);

                } else {

                    resolve(`${fileName} deleted`);

                }

            });

        } else {

            resolve(`${fileName} doesn't exist`);

        }

    });

}

serverApp.post('/', (request, response) => {

    let directoryName = request.body.directory;
    let files = request.body.files;

    if (fs.existsSync(path.join(__dirname, directoryName))) {

        let allPromises = files.map((file, fileIndex, files) => {

            return fileCreater(file, directoryName);
            
        });

        Promise.allSettled(allPromises).then((message) => {

            response.send(message);

        }).catch((message) => {

            console.error(message);

        });

    } else {

        directoryCreater(directoryName).then((data) => {

            console.log(data);

        }).catch((data) => {

            console.error(data);

        });

        let allPromises = files.map((file, fileIndex, files) => {

            fileCreater(file, directoryName);            

        });

        Promise.allSettled(allPromises).then((message) => {

            response.send(message);

        }).catch((message) => {

            console.error(message);

        });

    }

});

serverApp.delete('/', (request, response) => {

    let deleteDirectory = request.body.directory;
    let deleteFiles = request.body.files;

    if (fs.existsSync(path.join(__dirname, deleteDirectory))) {

        let allPromises = deleteFiles.map((file, fileIndex, files) => {

            return fileDeleter(`${file}.json`, deleteDirectory);
            
        });

        Promise.allSettled(allPromises).then((message) => {

            response.send(message);

        });

    } else {

        response.send(`${deleteDirectory} is not available in database`);

    }

});

serverApp.listen(PORT, () => {

    console.log(`listening server on port ${PORT}`);

});